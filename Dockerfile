
FROM python:latest

COPY router /app/router
COPY main.py /app/app.py
COPY entrypoint.sh /app/entrypoint.sh
COPY requirements.txt /app/requirements.txt
CMD pip install --upgrade pip
CMD pip install -r /app/requirements.txt
