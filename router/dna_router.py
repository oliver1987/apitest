from fastapi import APIRouter

router = APIRouter()

@router.get("/test")
def HelloApi():
    return {"Hello": "World"}

@router.post("/addGenom")
def add_genom(genom):
    return genom

@router.get("/{id}")
def get_genom(id:int):
    return id

@router.get("/getall")
def get_all_genom(id:int):
    return id